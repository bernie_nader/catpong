# README #

### What is this repository for? ###

* Cat Pong was a game I made based on a pong tutorial to teach myself Unity game development.
* 1.0

### How do I get set up? ###

* Put Assets and ProjectSettings folder in a top level folder and open it from Unity 4 as a project. This hasn't been tested in Unity 5, but my other games have broken when I've tried to use Unity 5, so use 4.

### Who do I talk to? ###

* Erica