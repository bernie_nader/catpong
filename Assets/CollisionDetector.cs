﻿using UnityEngine;
using System.Collections;

public class CollisionDetector : MonoBehaviour {

	Animator catanimator;
	bool isSuspicious;
	public CGameSetup gamesetup;

	void Start () 
	{
		GameObject gsetup = GameObject.FindWithTag ("GameController");
		if (gamesetup != null) {
			gamesetup = gsetup.GetComponent<CGameSetup> ();
		} else {
			Debug.Log("no reference");
		}
		catanimator = GetComponent<Animator>();
	}

	void OnCollisionEnter2D(Collision2D coll){
		if (catanimator != null) {
			isSuspicious = !isSuspicious;
			catanimator.SetBool ("Hit", isSuspicious);
			audio.Play ();
            //if it's a cat, detect it:
            if (coll.gameObject.tag.Equals("cat")){
                sendCatFightCounter();            
            }
		}
	}

    void sendCatFightCounter() {
        //and send a count of the times cats hit other cats 
        //to the game master script, to display to user
		gamesetup.addScore (1);

    }
	//void OnTriggerExit2D(Collider2D other){
	//	catanimator.SetBool("Hit", false); //changes too quickly - is there a way to let animation finish?
	//yep, try yield WaitForSeconds (animation[yourAnimation].length)
	//}

	void SWEET(string things){
		Debug.Log ("Sweet said " + things); 
	}

}
