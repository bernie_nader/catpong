﻿using UnityEngine;
using System.Collections;

public class CGameSetup : MonoBehaviour {
	//Reference the camera
	public Camera mainCam;
	
	//Reference the colliders we are going to adjust
	public BoxCollider2D topWall;
	public BoxCollider2D bottomWall;
	public BoxCollider2D leftWall;
	public BoxCollider2D rightWall;
	
	//Reference the players
	public Transform Player01;
	public Transform Player02;
	
	//auto generate kitties
	public Transform bouncekitty;

	public TextMesh scoretext;
	private int score;
	// Use this for initialization
	void Start () {
		score = 0;
		updateScore ();
		topWall.size = new Vector2 (mainCam.ScreenToWorldPoint (new Vector3 (Screen.width * 2f, 0f, 0f)).x, 1f);
		topWall.center = new Vector2 (0f, mainCam.ScreenToWorldPoint (new Vector3 ( 0f, Screen.height, 0f)).y + 0.5f);
		
		bottomWall.size = new Vector2 (mainCam.ScreenToWorldPoint (new Vector3 (Screen.width * 2, 0f, 0f)).x, 1f);
		bottomWall.center = new Vector2 (0f, mainCam.ScreenToWorldPoint (new Vector3( 0f, 0f, 0f)).y - 0.5f);
		
		leftWall.size = new Vector2(1f, mainCam.ScreenToWorldPoint(new Vector3(0f, Screen.height*2f, 0f)).y);;
		leftWall.center = new Vector2(mainCam.ScreenToWorldPoint(new Vector3(0f, 0f, 0f)).x - 0.5f, 0f);
		
		rightWall.size = new Vector2(1f, mainCam.ScreenToWorldPoint(new Vector3(0f, Screen.height*2f, 0f)).y);
		rightWall.center = new Vector2(mainCam.ScreenToWorldPoint(new Vector3(Screen.width, 0f, 0f)).x + 0.5f, 0f);
		
		//Move the players to a fixed distance from the edges of the screen:
		Vector3 p1 = Player01.position;
		p1.x = mainCam.ScreenToWorldPoint (new Vector3 (75f, 0f, 0f)).x;
		Player01.position = p1;
		Vector3 p2 = Player02.position;
		p2.x = mainCam.ScreenToWorldPoint (new Vector3 (Screen.width -75f, 0f, 0f)).x;
		Player02.position = p2;
		
		//audio.clip = myClip;
		audio.Play();
		
		//Experiment with spawning:
		for (var x = 0; x < 5; x++) {
			Instantiate(bouncekitty, new Vector3(x, 0, 0), Quaternion.identity);
		}
	}

	void updateScore(){
		scoretext.text = "Score: " + score;
	}

	public void addScore(int scoreinc){
		score += scoreinc;
		updateScore ();
	}
}
